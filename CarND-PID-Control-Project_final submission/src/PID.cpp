#include "PID.h"

#include <algorithm>
// using namespace std;

/*
* TODO: Complete the PID class.
*/

PID::PID() {}

PID::~PID() {}

void PID::Init(double Kp, double Ki, double Kd) {
  this->Kp = Kp;
  this->Ki = Ki;
  this->Kd = Kd;
  

  p_error = 0.0;
  i_error = 0.0;
  d_error = 0.0;

  // Previous cte.
  prev_cte = 0.0;

  
}

void PID::UpdateError(double cte) {
  //error for proportional.
  p_error = cte;

  // error for integral component.
  i_error += cte;

  //error for differential component.
  d_error = cte - prev_cte;
  prev_cte = cte;

  

  
}

double PID::TotalError() {
  return p_error * Kp + i_error * Ki + d_error * Kd;
}