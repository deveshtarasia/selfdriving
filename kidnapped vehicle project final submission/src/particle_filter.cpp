/**
 * particle_filter.cpp
 *
 * Created on: Dec 12, 2016
 * Author: Tiffany Huang
 */

#include "particle_filter.h"

#include <math.h>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <numeric>
#include <random>
#include <string>
#include <vector>

#include "helper_functions.h"
//static default_random_engine gen;
using namespace std;

using std::string;
using std::vector;

void ParticleFilter::init(double x, double y, double theta, double std[]) {
  /**
   * TODO: Set the number of particles. Initialize all particles to 
   *   first position (based on estimates of x, y, theta and their uncertainties
   *   from GPS) and all weights to 1. 
   * TODO: Add random Gaussian noise to each particle.
   * NOTE: Consult particle_filter.h for more information about this method 
   *   (and others in this file).
   */
  
  num_particles = 100;  // TODO: Set the number of particles
  //standard deviation
  double std_x = std[0];
  double std_y = std[1];
  double std_theta = std[2];
  default_random_engine gen;
  //initiliazing normal distribution
  normal_distribution<double> dist_x(x, std_x);
  normal_distribution<double> dist_y(y, std_y);
  normal_distribution<double> dist_theta(theta, std_theta);

  
  for (int i = 0; i < num_particles; i++) {

    Particle par;
    par.id = i;
    par.x = dist_x(gen);
    par.y = dist_y(gen);
    par.theta = dist_theta(gen);
    par.weight = 1.0;

    particles.push_back(par);
	}

  // The filter initialized.
  is_initialized = true;
  

}

void ParticleFilter::prediction(double delta_t, double std_pos[], 
                                double velocity, double yaw_rate) {
  /**
   * TODO: Add measurements to each particle and add random Gaussian noise.
   * NOTE: When adding noise you may find std::normal_distribution 
   *   and std::default_random_engine useful.
   *  http://en.cppreference.com/w/cpp/numeric/random/normal_distribution
   *  http://www.cplusplus.com/reference/random/default_random_engine/
   */

  //setting standard deviation
  double std_x = std_pos[0];
  double std_y = std_pos[1];
  double std_theta = std_pos[2];
  //create normal distributions
  default_random_engine gen;
  normal_distribution<double> dist_x(0, std_x);
  normal_distribution<double> dist_y(0, std_y);
  normal_distribution<double> dist_theta(0, std_theta);
    
  for(int i=0; i<num_particles;i++){

        
    double par_x = particles[i].x;
    double par_y = particles[i].y;
    double theta = particles[i].theta;

        
    if(fabs(yaw_rate)>0.001){
      
      particles[i].x = par_x + velocity/yaw_rate * (sin(theta+yaw_rate*delta_t) - sin(theta));
      particles[i].y = par_y + velocity/yaw_rate * (cos(theta) - cos(theta+yaw_rate*delta_t));
      particles[i].theta = theta + yaw_rate*delta_t;
      } 
    else{              
      particles[i].x = par_x + velocity*delta_t*cos(theta);
      particles[i].y = par_y + velocity*delta_t*sin(theta);
      }
        
  //add noise      
  particles[i].x = particles[i].x + dist_x(gen);
  particles[i].y = particles[i].y + dist_y(gen);
  particles[i].theta = particles[i].theta + dist_theta(gen);
  }
}

void ParticleFilter::dataAssociation(vector<LandmarkObs> predicted, 
                                     vector<LandmarkObs>& observations) {
  /**
   * TODO: Find the predicted measurement that is closest to each 
   *   observed measurement and assign the observed measurement to this 
   *   particular landmark.
   * NOTE: this method will NOT be called by the grading code. But you will 
   *   probably find it useful to implement this method and use it as a helper 
   *   during the updateWeights phase.
   */
  

  for (int i = 0; i < observations.size(); i++) {
    double obs_x = observations[i].x;
    double obs_y = observations[i].y;
    int id = -1;
     
    // Initialize min distance as a really big number.
    double min_dist = numeric_limits<double>::max();

    // Initialize the found map in something not possible.
    
    for (int j = 0; j < predicted.size(); j++ ) { 

      double pred_x = predicted[j].x;
      double pred_y = predicted[j].y;
      int pred_id = predicted[j].id;

      

      double distance = dist(obs_x, obs_y, pred_x, pred_y);

      

      

      // If the "distance" is less than min, stored the id and update min.
      if ( distance < min_dist ) {
        min_dist = distance;
        id = predicted[j].id;
      }
    }

    // Update the observation identifier.
    observations[i].id = id;
  }

}

void ParticleFilter::updateWeights(double sensor_range, double std_landmark[], 
                                   const vector<LandmarkObs> &observations, 
                                   const Map &map_landmarks) {
  /**
   * TODO: Update the weights of each particle using a mult-variate Gaussian 
   *   distribution. You can read more about this distribution here: 
   *   https://en.wikipedia.org/wiki/Multivariate_normal_distribution
   * NOTE: The observations are given in the VEHICLE'S coordinate system. 
   *   Your particles are located according to the MAP'S coordinate system. 
   *   You will need to transform between the two systems. Keep in mind that
   *   this transformation requires both rotation AND translation (but no scaling).
   *   The following is a good resource for the theory:
   *   https://www.willamette.edu/~gorr/classes/GeneralGraphics/Transforms/transforms2d.htm
   *   and the following is a good resource for the actual equation to implement
   *   (look at equation 3.33) http://planning.cs.uiuc.edu/node99.html
   */
  double s_x = std_landmark[0];
  double s_y = std_landmark[1];

  for (int i = 0; i < num_particles; i++) {

    double par_x = particles[i].x;
    double par_y = particles[i].y;
    double theta = particles[i].theta;
    

    vector<LandmarkObs> obs_particles;

    for( unsigned int j = 0; j < map_landmarks.landmark_list.size(); j++) {

      float obs_x = map_landmarks.landmark_list[j].x_f;
      float obs_y = map_landmarks.landmark_list[j].y_f;
      int id = map_landmarks.landmark_list[j].id_i;

      double distance = dist(par_x,par_y,obs_x,obs_y);
      

      if(fabs(distance<=sensor_range) ){
        obs_particles.push_back(LandmarkObs{ id, obs_x, obs_y });

      }
      
    }

    // Transform vehicle to map cordinates.
    vector<LandmarkObs> transform_matrix;

    for(unsigned int j = 0; j < observations.size(); j++) {

      double t_x = cos(theta)*observations[j].x - sin(theta)*observations[j].y + par_x;
      double t_y = sin(theta)*observations[j].x + cos(theta)*observations[j].y + par_y;

      transform_matrix.push_back(LandmarkObs{ observations[j].id, t_x, t_y });
    }

    // Observation association to landmark.
    dataAssociation(obs_particles, transform_matrix);

    // Reseting weight.
    particles[i].weight = 1.0;
    
    for(unsigned int k = 0; k < transform_matrix.size(); k++) {
      double p_x,p_y;

      double transform_x = transform_matrix[k].x;
      double transform_y = transform_matrix[k].y;
      int transform_id = transform_matrix[k].id;

      for(int l=0 ;l<obs_particles.size();l++){
        if(obs_particles[l].id == transform_id){
           p_x = obs_particles[l].x;
           p_y = obs_particles[l].y;
          
        }

      }
      double final_no = ( 1/(2*M_PI*s_x*s_y)) * exp( -( pow(p_x-transform_x,2)/(2*pow(s_x, 2)) + (pow(p_y-transform_y,2)/(2*pow(s_y, 2))) ) );
      particles[i].weight = particles[i].weight*final_no;     
      
    }
  }

}

void ParticleFilter::resample() {
  /**
   * TODO: Resample particles with replacement with probability proportional 
   *   to their weight. 
   * NOTE: You may find std::discrete_distribution helpful here.
   *   http://en.cppreference.com/w/cpp/numeric/random/discrete_distribution
   */
  vector<double> weights;
  default_random_engine gen;
  vector<Particle> resample_particles;
  
  for(int i = 0; i < num_particles; i++) {
    weights.push_back(particles[i].weight);
    
  }
  double max_weight = *max_element(weights.begin(), weights.end());

  
  uniform_real_distribution<double> random_weight(0.0, max_weight);
  uniform_int_distribution<int> random_index(0, num_particles - 1);

  
  int index = random_index(gen);

  double beta = 0.0;

  // the wheel
  
  for(int i = 0; i < num_particles; i++) {
    beta = beta + random_weight(gen) * 2.0;
    while( beta > weights[index]) {
      beta = beta -weights[index];
      index = (index + 1) % num_particles;
    }
    resample_particles.push_back(particles[index]);
  }
  

  particles = resample_particles;


}

void ParticleFilter::SetAssociations(Particle& particle, 
                                     const vector<int>& associations, 
                                     const vector<double>& sense_x, 
                                     const vector<double>& sense_y) {
  // particle: the particle to which assign each listed association, 
  //   and association's (x,y) world coordinates mapping
  // associations: The landmark id that goes along with each listed association
  // sense_x: the associations x mapping already converted to world coordinates
  // sense_y: the associations y mapping already converted to world coordinates
  particle.associations= associations;
  particle.sense_x = sense_x;
  particle.sense_y = sense_y;
}

string ParticleFilter::getAssociations(Particle best) {
  vector<int> v = best.associations;
  std::stringstream ss;
  copy(v.begin(), v.end(), std::ostream_iterator<int>(ss, " "));
  string s = ss.str();
  s = s.substr(0, s.length()-1);  // get rid of the trailing space
  return s;
}

string ParticleFilter::getSenseCoord(Particle best, string coord) {
  vector<double> v;

  if (coord == "X") {
    v = best.sense_x;
  } else {
    v = best.sense_y;
  }

  std::stringstream ss;
  copy(v.begin(), v.end(), std::ostream_iterator<float>(ss, " "));
  string s = ss.str();
  s = s.substr(0, s.length()-1);  // get rid of the trailing space
  return s;
}